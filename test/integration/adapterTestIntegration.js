/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-ringcentral',
      type: 'Ringcentral',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Ringcentral = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Ringcentral Adapter Test', () => {
  describe('Ringcentral Class Tests', () => {
    const a = new Ringcentral(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const restapiPostRestapiOauthAuthorizeBodyParam = {
      response_type: 'string',
      client_id: 'string',
      redirect_uri: 'string',
      state: 'string'
    };
    describe('#postRestapiOauthAuthorize - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapiOauthAuthorize(restapiPostRestapiOauthAuthorizeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'postRestapiOauthAuthorize', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiPostRestapiOauthRevokeBodyParam = {
      token: 'string'
    };
    describe('#postRestapiOauthRevoke - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapiOauthRevoke(restapiPostRestapiOauthRevokeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'postRestapiOauthRevoke', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiPostRestapiOauthTokenBodyParam = {
      grant_type: 'string',
      access_token_ttl: 7,
      refresh_token_ttl: 6,
      username: 'string',
      extension: 'string',
      password: 'string',
      scope: 'string',
      endpoint_id: 'string'
    };
    describe('#postRestapiOauthToken - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapiOauthToken(restapiPostRestapiOauthTokenBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'postRestapiOauthToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiAccountId = 'fakedata';
    const restapiExtensionId = 'fakedata';
    const restapiPostRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactBodyParam = {
      id: 9,
      url: 'string',
      availability: 'Deleted',
      firstName: 'string',
      lastName: 'string',
      middleName: 'string',
      nickName: 'string',
      company: 'string',
      jobTitle: 'string',
      homePhone: 'string',
      homePhone2: 'string',
      businessPhone: 'string',
      businessPhone2: 'string',
      mobilePhone: 'string',
      businessFax: 'string',
      companyPhone: 'string',
      assistantPhone: 'string',
      carPhone: 'string',
      otherPhone: 'string',
      otherFax: 'string',
      callbackPhone: 'string',
      email: 'string',
      email2: 'string',
      email3: 'string',
      homeAddress: {
        country: 'string',
        state: 'string',
        city: 'string',
        street: 'string',
        zip: 'string'
      },
      businessAddress: {
        country: 'string',
        state: 'string',
        city: 'string',
        street: 'string',
        zip: 'string'
      },
      otherAddress: {
        country: 'string',
        state: 'string',
        city: 'string',
        street: 'string',
        zip: 'string'
      },
      birthday: 'string',
      webPage: 'string',
      notes: 'string'
    };
    describe('#postRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContact - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContact(restapiAccountId, restapiExtensionId, restapiPostRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'postRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContact', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiPostRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBodyParam = {
      id: 'string',
      uri: 'string',
      name: 'string',
      phoneNumber: 'string'
    };
    describe('#postRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumber - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumber(restapiAccountId, restapiExtensionId, restapiPostRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'postRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumber', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiPostRestapiV10AccountAccountIdExtensionExtensionIdCompanyPagerBodyParam = {
      from: {
        phoneNumber: 'string',
        extensionNumber: 'string',
        location: 'string',
        name: 'string'
      },
      replyOn: 1,
      text: 'string',
      to: [
        {
          phoneNumber: 'string',
          extensionNumber: 'string',
          location: 'string',
          name: 'string'
        }
      ]
    };
    describe('#postRestapiV10AccountAccountIdExtensionExtensionIdCompanyPager - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdCompanyPager(restapiAccountId, restapiExtensionId, restapiPostRestapiV10AccountAccountIdExtensionExtensionIdCompanyPagerBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'postRestapiV10AccountAccountIdExtensionExtensionIdCompanyPager', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiPostRestapiV10AccountAccountIdExtensionExtensionIdFaxBodyParam = {
      to: [
        {
          phoneNumber: 'string',
          extensionNumber: 'string',
          location: 'string',
          name: 'string'
        }
      ],
      resolution: 'Low',
      sendTime: 'string',
      coverIndex: 3,
      coverPageText: 'string',
      originalMessageId: 'string'
    };
    describe('#postRestapiV10AccountAccountIdExtensionExtensionIdFax - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdFax(restapiAccountId, restapiExtensionId, restapiPostRestapiV10AccountAccountIdExtensionExtensionIdFaxBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'postRestapiV10AccountAccountIdExtensionExtensionIdFax', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiPostRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumberBodyParam = {
      phoneNumber: 'string',
      label: 'string'
    };
    describe('#postRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumber - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumber(restapiAccountId, restapiExtensionId, restapiPostRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumberBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'postRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumber', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiPostRestapiV10AccountAccountIdExtensionExtensionIdProfileImageBodyParam = {};
    describe('#postRestapiV10AccountAccountIdExtensionExtensionIdProfileImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdProfileImage(restapiAccountId, restapiExtensionId, restapiPostRestapiV10AccountAccountIdExtensionExtensionIdProfileImageBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'postRestapiV10AccountAccountIdExtensionExtensionIdProfileImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiPostRestapiV10AccountAccountIdExtensionExtensionIdRingoutBodyParam = {
      from: {
        phoneNumber: 'string',
        forwardingNumberId: 'string'
      },
      to: {
        phoneNumber: 'string'
      },
      callerId: {
        phoneNumber: 'string'
      },
      playPrompt: true,
      country: {
        id: 'string'
      }
    };
    describe('#postRestapiV10AccountAccountIdExtensionExtensionIdRingout - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdRingout(restapiAccountId, restapiExtensionId, restapiPostRestapiV10AccountAccountIdExtensionExtensionIdRingoutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'postRestapiV10AccountAccountIdExtensionExtensionIdRingout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiPostRestapiV10AccountAccountIdExtensionExtensionIdSmsBodyParam = {
      from: {
        phoneNumber: 'string',
        extensionNumber: 'string',
        location: 'string',
        name: 'string'
      },
      to: [
        {
          phoneNumber: 'string',
          extensionNumber: 'string',
          location: 'string',
          name: 'string'
        }
      ],
      text: 'string'
    };
    describe('#postRestapiV10AccountAccountIdExtensionExtensionIdSms - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdSms(restapiAccountId, restapiExtensionId, restapiPostRestapiV10AccountAccountIdExtensionExtensionIdSmsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'postRestapiV10AccountAccountIdExtensionExtensionIdSms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiPostRestapiV10AccountAccountIdOrderBodyParam = {
      devices: [
        {
          id: 'string',
          uri: 'string',
          sku: 'string',
          type: 'OtherPhone',
          name: 'string',
          serial: 'string',
          computerName: 'string',
          model: {
            id: 'string',
            name: 'string',
            addons: [
              {
                id: 'string',
                count: 6
              }
            ]
          },
          extension: {
            id: 'string',
            uri: 'string',
            extensionNumber: 'string',
            partnerId: 'string'
          },
          emergencyServiceAddress: {
            customerName: 'string',
            street: 'string',
            street2: 'string',
            city: 'string',
            state: 'string',
            zip: 'string',
            country: 'string'
          },
          phoneLines: {
            lineType: 'StandaloneFree',
            phoneInfo: {
              id: 7,
              country: {
                id: 'string',
                uri: 'string',
                name: 'string'
              },
              location: 'string',
              paymentType: 'External',
              phoneNumber: 'string',
              status: 'string',
              type: 'VoiceOnly',
              usageType: 'CompanyFaxNumber'
            }
          },
          shipping: {
            status: 'Accepted',
            carrier: 'string',
            trackingNumber: 'string',
            method: [
              {
                id: '3',
                name: 'Ground'
              }
            ],
            address: [
              {
                customerName: 'string',
                street: 'string',
                street2: 'string',
                city: 'string',
                state: 'string',
                zip: 'string',
                country: 'string'
              }
            ]
          },
          boxBillingId: 7
        }
      ]
    };
    describe('#postRestapiV10AccountAccountIdOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapiV10AccountAccountIdOrder(restapiAccountId, restapiPostRestapiV10AccountAccountIdOrderBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'postRestapiV10AccountAccountIdOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiPostRestapiV10NumberParserParseBodyParam = {
      originalStrings: [
        'string'
      ]
    };
    describe('#postRestapiV10NumberParserParse - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapiV10NumberParserParse(null, null, restapiPostRestapiV10NumberParserParseBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'postRestapiV10NumberParserParse', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiCountryId = 'fakedata';
    describe('#postRestapiV10NumberPoolLookup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapiV10NumberPoolLookup(null, null, restapiCountryId, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'postRestapiV10NumberPoolLookup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiPostRestapiV10NumberPoolReserveBodyParam = {
      records: [
        {
          phoneNumber: 'string',
          reservedTill: 'string'
        }
      ]
    };
    describe('#postRestapiV10NumberPoolReserve - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapiV10NumberPoolReserve(restapiPostRestapiV10NumberPoolReserveBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'postRestapiV10NumberPoolReserve', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiPostRestapiV10SubscriptionBodyParam = {
      eventFilters: [
        'string'
      ],
      deliveryMode: {
        transportType: 'PubNub',
        encryption: true
      }
    };
    describe('#postRestapiV10Subscription - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapiV10Subscription(null, restapiPostRestapiV10SubscriptionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'postRestapiV10Subscription', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapi', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountId(restapiAccountId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdActiveCalls - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdActiveCalls(restapiAccountId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdActiveCalls', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiPutRestapiV10AccountAccountIdBusinessAddressBodyParam = {
      company: 'string',
      email: 'string',
      businessAddress: {
        country: 'string',
        state: 'string',
        city: 'string',
        street: 'string',
        zip: 'string'
      }
    };
    describe('#putRestapiV10AccountAccountIdBusinessAddress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRestapiV10AccountAccountIdBusinessAddress(restapiAccountId, restapiPutRestapiV10AccountAccountIdBusinessAddressBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'putRestapiV10AccountAccountIdBusinessAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdBusinessAddress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdBusinessAddress(restapiAccountId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdBusinessAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdCallLog - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdCallLog(restapiAccountId, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdCallLog', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiCallLogId = 555;
    describe('#getRestapiV10AccountAccountIdCallLogCallLogId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdCallLogCallLogId(restapiAccountId, restapiCallLogId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdCallLogCallLogId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiDepartmentId = 555;
    describe('#getRestapiV10AccountAccountIdDepartmentDepartmentIdMembers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdDepartmentDepartmentIdMembers(restapiAccountId, restapiDepartmentId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdDepartmentDepartmentIdMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdDevice(restapiAccountId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiDeviceId = 555;
    describe('#getRestapiV10AccountAccountIdDeviceDeviceId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdDeviceDeviceId(restapiAccountId, restapiDeviceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdDeviceDeviceId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtension - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtension(restapiAccountId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtension', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiPutRestapiV10AccountAccountIdExtensionExtensionIdBodyParam = {};
    describe('#putRestapiV10AccountAccountIdExtensionExtensionId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRestapiV10AccountAccountIdExtensionExtensionId(restapiAccountId, restapiExtensionId, restapiPutRestapiV10AccountAccountIdExtensionExtensionIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'putRestapiV10AccountAccountIdExtensionExtensionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionId(restapiAccountId, restapiExtensionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdActiveCalls - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdActiveCalls(restapiAccountId, restapiExtensionId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdActiveCalls', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookSync - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookSync(restapiAccountId, restapiExtensionId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookSync', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContact - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContact(restapiAccountId, restapiExtensionId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContact', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiContactId = 555;
    const restapiPutRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactIdBodyParam = {
      id: 3,
      url: 'string',
      availability: 'Purged',
      firstName: 'string',
      lastName: 'string',
      middleName: 'string',
      nickName: 'string',
      company: 'string',
      jobTitle: 'string',
      homePhone: 'string',
      homePhone2: 'string',
      businessPhone: 'string',
      businessPhone2: 'string',
      mobilePhone: 'string',
      businessFax: 'string',
      companyPhone: 'string',
      assistantPhone: 'string',
      carPhone: 'string',
      otherPhone: 'string',
      otherFax: 'string',
      callbackPhone: 'string',
      email: 'string',
      email2: 'string',
      email3: 'string',
      homeAddress: {
        country: 'string',
        state: 'string',
        city: 'string',
        street: 'string',
        zip: 'string'
      },
      businessAddress: {
        country: 'string',
        state: 'string',
        city: 'string',
        street: 'string',
        zip: 'string'
      },
      otherAddress: {
        country: 'string',
        state: 'string',
        city: 'string',
        street: 'string',
        zip: 'string'
      },
      birthday: 'string',
      webPage: 'string',
      notes: 'string'
    };
    describe('#putRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId(restapiAccountId, restapiExtensionId, restapiContactId, restapiPutRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'putRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId(restapiAccountId, restapiExtensionId, restapiContactId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookGroup(restapiAccountId, restapiExtensionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiAnsweringRuleId = 'fakedata';
    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdAnsweringRuleAnsweringRuleId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdAnsweringRuleAnsweringRuleId(restapiAccountId, restapiExtensionId, restapiAnsweringRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdAnsweringRuleAnsweringRuleId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumber - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumber(restapiAccountId, restapiExtensionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumber', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiBlockedNumberId = 555;
    const restapiPutRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberIdBodyParam = {
      id: 'string',
      uri: 'string',
      name: 'string',
      phoneNumber: 'string'
    };
    describe('#putRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId(restapiAccountId, restapiExtensionId, restapiBlockedNumberId, restapiPutRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'putRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId(restapiAccountId, restapiExtensionId, restapiBlockedNumberId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdBusinessHours - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdBusinessHours(restapiAccountId, restapiExtensionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdBusinessHours', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdCallLog - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdCallLog(restapiAccountId, restapiExtensionId, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdCallLog', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdCallLogSync - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdCallLogSync(restapiAccountId, restapiExtensionId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdCallLogSync', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdCallLogCallLogId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdCallLogCallLogId(restapiAccountId, restapiExtensionId, restapiCallLogId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdCallLogCallLogId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiPutRestapiV10AccountAccountIdExtensionExtensionIdConferencingBodyParam = {
      phoneNumbers: [
        {
          phoneNumber: 'string',
          default: true
        }
      ],
      allowJoinBeforeHost: false
    };
    describe('#putRestapiV10AccountAccountIdExtensionExtensionIdConferencing - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRestapiV10AccountAccountIdExtensionExtensionIdConferencing(restapiAccountId, restapiExtensionId, restapiPutRestapiV10AccountAccountIdExtensionExtensionIdConferencingBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'putRestapiV10AccountAccountIdExtensionExtensionIdConferencing', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdConferencing - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdConferencing(restapiAccountId, restapiExtensionId, restapiCountryId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdConferencing', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdDevice(restapiAccountId, restapiExtensionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumber - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumber(restapiAccountId, restapiExtensionId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumber', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdGrant - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdGrant(restapiAccountId, restapiExtensionId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdGrant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdMessageStore - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdMessageStore(restapiAccountId, restapiExtensionId, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdMessageStore', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiMessageId = 555;
    const restapiPutRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageIdBodyParam = {
      readStatus: 'Unread'
    };
    describe('#putRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId(restapiAccountId, restapiExtensionId, restapiMessageId, restapiPutRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'putRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId(restapiAccountId, restapiExtensionId, restapiMessageId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiAttachmentId = 555;
    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageIdContentAttachmentId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageIdContentAttachmentId(restapiAccountId, restapiExtensionId, restapiMessageId, restapiAttachmentId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageIdContentAttachmentId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdMessageSync - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdMessageSync(restapiAccountId, restapiExtensionId, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdMessageSync', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdPhoneNumber - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdPhoneNumber(restapiAccountId, restapiExtensionId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdPhoneNumber', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdPresence - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdPresence(restapiAccountId, restapiExtensionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdPresence', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiPutRestapiV10AccountAccountIdExtensionExtensionIdProfileImageBodyParam = {};
    describe('#putRestapiV10AccountAccountIdExtensionExtensionIdProfileImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRestapiV10AccountAccountIdExtensionExtensionIdProfileImage(restapiAccountId, restapiExtensionId, restapiPutRestapiV10AccountAccountIdExtensionExtensionIdProfileImageBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'putRestapiV10AccountAccountIdExtensionExtensionIdProfileImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdProfileImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdProfileImage(restapiAccountId, restapiExtensionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdProfileImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiScaleSize = 'fakedata';
    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdProfileImageScaleSize - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdProfileImageScaleSize(restapiAccountId, restapiExtensionId, restapiScaleSize, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdProfileImageScaleSize', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiRingoutId = 555;
    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId(restapiAccountId, restapiExtensionId, restapiRingoutId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiOrderId = 555;
    describe('#getRestapiV10AccountAccountIdOrderOrderId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdOrderOrderId(restapiAccountId, restapiOrderId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdOrderOrderId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdPhoneNumber - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdPhoneNumber(restapiAccountId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdPhoneNumber', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiPhoneNumberId = 555;
    describe('#getRestapiV10AccountAccountIdPhoneNumberPhoneNumberId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdPhoneNumberPhoneNumberId(restapiAccountId, restapiPhoneNumberId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdPhoneNumberPhoneNumberId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiRecordingId = 555;
    describe('#getRestapiV10AccountAccountIdRecordingRecordingId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdRecordingRecordingId(restapiAccountId, restapiRecordingId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdRecordingRecordingId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdRecordingRecordingIdContent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdRecordingRecordingIdContent(restapiAccountId, restapiRecordingId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdRecordingRecordingIdContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdServiceInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10AccountAccountIdServiceInfo(restapiAccountId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10AccountAccountIdServiceInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiCustomDataKey = 'fakedata';
    const restapiPutRestapiV10ClientInfoCustomDataCustomDataKeyBodyParam = {
      id: 'string',
      value: 'string'
    };
    describe('#putRestapiV10ClientInfoCustomDataCustomDataKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRestapiV10ClientInfoCustomDataCustomDataKey(restapiCustomDataKey, restapiPutRestapiV10ClientInfoCustomDataCustomDataKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'putRestapiV10ClientInfoCustomDataCustomDataKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10DictionaryCountry - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10DictionaryCountry(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10DictionaryCountry', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10DictionaryCountryCountryId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10DictionaryCountryCountryId(restapiCountryId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10DictionaryCountryCountryId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10DictionaryLanguage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10DictionaryLanguage((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10DictionaryLanguage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiLanguageId = 555;
    describe('#getRestapiV10DictionaryLanguageLanguageId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10DictionaryLanguageLanguageId(restapiLanguageId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10DictionaryLanguageLanguageId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiStateId = 'fakedata';
    describe('#getRestapiV10DictionaryLocation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10DictionaryLocation(null, null, null, restapiStateId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10DictionaryLocation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10DictionaryState - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10DictionaryState(restapiCountryId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10DictionaryState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10DictionaryStateStateId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10DictionaryStateStateId(restapiStateId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10DictionaryStateStateId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10DictionaryTimezone - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10DictionaryTimezone(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10DictionaryTimezone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiTimezoneId = 555;
    describe('#getRestapiV10DictionaryTimezoneTimezoneId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10DictionaryTimezoneTimezoneId(restapiTimezoneId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10DictionaryTimezoneTimezoneId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restapiSubscriptionId = 'fakedata';
    const restapiPutRestapiV10SubscriptionSubscriptionIdBodyParam = {
      eventFilters: [
        'string'
      ]
    };
    describe('#putRestapiV10SubscriptionSubscriptionId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRestapiV10SubscriptionSubscriptionId(restapiSubscriptionId, null, restapiPutRestapiV10SubscriptionSubscriptionIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'putRestapiV10SubscriptionSubscriptionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10SubscriptionSubscriptionId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapiV10SubscriptionSubscriptionId(restapiSubscriptionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'getRestapiV10SubscriptionSubscriptionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId(restapiAccountId, restapiExtensionId, restapiContactId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'deleteRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId(restapiAccountId, restapiExtensionId, restapiBlockedNumberId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'deleteRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapiV10AccountAccountIdExtensionExtensionIdCallLog - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapiV10AccountAccountIdExtensionExtensionIdCallLog(restapiAccountId, restapiExtensionId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'deleteRestapiV10AccountAccountIdExtensionExtensionIdCallLog', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId(restapiAccountId, restapiExtensionId, restapiMessageId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'deleteRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId(restapiAccountId, restapiExtensionId, restapiRingoutId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'deleteRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapiV10SubscriptionSubscriptionId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapiV10SubscriptionSubscriptionId(restapiSubscriptionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ringcentral-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Restapi', 'deleteRestapiV10SubscriptionSubscriptionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
