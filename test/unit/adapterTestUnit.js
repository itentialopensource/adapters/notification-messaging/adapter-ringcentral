/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-ringcentral',
      type: 'Ringcentral',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const Ringcentral = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Ringcentral Adapter Test', () => {
  describe('Ringcentral Class Tests', () => {
    const a = new Ringcentral(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('ringcentral'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('ringcentral'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('Ringcentral', pronghornDotJson.export);
          assert.equal('Ringcentral', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-ringcentral', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('ringcentral'));
          assert.equal('Ringcentral', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-ringcentral', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-ringcentral', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getRestapi - errors', () => {
      it('should have a getRestapi function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapiOauthAuthorize - errors', () => {
      it('should have a postRestapiOauthAuthorize function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapiOauthAuthorize === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapiOauthRevoke - errors', () => {
      it('should have a postRestapiOauthRevoke function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapiOauthRevoke === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapiOauthToken - errors', () => {
      it('should have a postRestapiOauthToken function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapiOauthToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10 - errors', () => {
      it('should have a getRestapiV10 function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountId - errors', () => {
      it('should have a getRestapiV10AccountAccountId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountId(null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdActiveCalls - errors', () => {
      it('should have a getRestapiV10AccountAccountIdActiveCalls function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdActiveCalls === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdActiveCalls(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdActiveCalls', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdBusinessAddress - errors', () => {
      it('should have a getRestapiV10AccountAccountIdBusinessAddress function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdBusinessAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdBusinessAddress(null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdBusinessAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapiV10AccountAccountIdBusinessAddress - errors', () => {
      it('should have a putRestapiV10AccountAccountIdBusinessAddress function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapiV10AccountAccountIdBusinessAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.putRestapiV10AccountAccountIdBusinessAddress(null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-putRestapiV10AccountAccountIdBusinessAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdCallLog - errors', () => {
      it('should have a getRestapiV10AccountAccountIdCallLog function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdCallLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdCallLog(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdCallLog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdCallLogCallLogId - errors', () => {
      it('should have a getRestapiV10AccountAccountIdCallLogCallLogId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdCallLogCallLogId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdCallLogCallLogId(null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdCallLogCallLogId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing callLogId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdCallLogCallLogId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'callLogId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdCallLogCallLogId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdDepartmentDepartmentIdMembers - errors', () => {
      it('should have a getRestapiV10AccountAccountIdDepartmentDepartmentIdMembers function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdDepartmentDepartmentIdMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdDepartmentDepartmentIdMembers(null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdDepartmentDepartmentIdMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing departmentId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdDepartmentDepartmentIdMembers('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'departmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdDepartmentDepartmentIdMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdDevice - errors', () => {
      it('should have a getRestapiV10AccountAccountIdDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdDevice(null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdDeviceDeviceId - errors', () => {
      it('should have a getRestapiV10AccountAccountIdDeviceDeviceId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdDeviceDeviceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdDeviceDeviceId(null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdDeviceDeviceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdDeviceDeviceId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdDeviceDeviceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtension - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtension function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtension === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtension(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtension', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionId - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionId(null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapiV10AccountAccountIdExtensionExtensionId - errors', () => {
      it('should have a putRestapiV10AccountAccountIdExtensionExtensionId function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapiV10AccountAccountIdExtensionExtensionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.putRestapiV10AccountAccountIdExtensionExtensionId(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-putRestapiV10AccountAccountIdExtensionExtensionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.putRestapiV10AccountAccountIdExtensionExtensionId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-putRestapiV10AccountAccountIdExtensionExtensionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdActiveCalls - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdActiveCalls function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdActiveCalls === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdActiveCalls(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdActiveCalls', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdActiveCalls('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdActiveCalls', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookSync - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookSync function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookSync === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookSync(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookSync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookSync('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookSync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContact - errors', () => {
      it('should have a postRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContact function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContact === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContact(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-postRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContact', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContact('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-postRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContact', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContact - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContact function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContact === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContact(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContact', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContact('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContact', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId - errors', () => {
      it('should have a deleteRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.deleteRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-deleteRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.deleteRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-deleteRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing contactId', (done) => {
        try {
          a.deleteRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'contactId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-deleteRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing contactId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'contactId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId - errors', () => {
      it('should have a putRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.putRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-putRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.putRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-putRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing contactId', (done) => {
        try {
          a.putRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'contactId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-putRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookGroup - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookGroup(null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdAnsweringRuleAnsweringRuleId - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdAnsweringRuleAnsweringRuleId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdAnsweringRuleAnsweringRuleId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdAnsweringRuleAnsweringRuleId(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdAnsweringRuleAnsweringRuleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdAnsweringRuleAnsweringRuleId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdAnsweringRuleAnsweringRuleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing answeringRuleId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdAnsweringRuleAnsweringRuleId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'answeringRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdAnsweringRuleAnsweringRuleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumber - errors', () => {
      it('should have a postRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumber function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumber === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumber(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-postRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumber', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumber('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-postRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumber', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumber - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumber function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumber === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumber(null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumber', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumber('fakeparam', null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumber', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId - errors', () => {
      it('should have a deleteRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.deleteRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-deleteRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.deleteRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-deleteRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing blockedNumberId', (done) => {
        try {
          a.deleteRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'blockedNumberId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-deleteRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing blockedNumberId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'blockedNumberId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId - errors', () => {
      it('should have a putRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.putRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-putRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.putRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-putRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing blockedNumberId', (done) => {
        try {
          a.putRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'blockedNumberId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-putRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdBusinessHours - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdBusinessHours function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdBusinessHours === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdBusinessHours(null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdBusinessHours', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdBusinessHours('fakeparam', null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdBusinessHours', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapiV10AccountAccountIdExtensionExtensionIdCallLog - errors', () => {
      it('should have a deleteRestapiV10AccountAccountIdExtensionExtensionIdCallLog function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapiV10AccountAccountIdExtensionExtensionIdCallLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.deleteRestapiV10AccountAccountIdExtensionExtensionIdCallLog(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-deleteRestapiV10AccountAccountIdExtensionExtensionIdCallLog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.deleteRestapiV10AccountAccountIdExtensionExtensionIdCallLog('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-deleteRestapiV10AccountAccountIdExtensionExtensionIdCallLog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdCallLog - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdCallLog function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdCallLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdCallLog(null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdCallLog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdCallLog('fakeparam', null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdCallLog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdCallLogSync - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdCallLogSync function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdCallLogSync === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdCallLogSync(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdCallLogSync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdCallLogSync('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdCallLogSync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdCallLogCallLogId - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdCallLogCallLogId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdCallLogCallLogId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdCallLogCallLogId(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdCallLogCallLogId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdCallLogCallLogId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdCallLogCallLogId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing callLogId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdCallLogCallLogId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'callLogId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdCallLogCallLogId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapiV10AccountAccountIdExtensionExtensionIdCompanyPager - errors', () => {
      it('should have a postRestapiV10AccountAccountIdExtensionExtensionIdCompanyPager function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapiV10AccountAccountIdExtensionExtensionIdCompanyPager === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdCompanyPager(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-postRestapiV10AccountAccountIdExtensionExtensionIdCompanyPager', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdCompanyPager('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-postRestapiV10AccountAccountIdExtensionExtensionIdCompanyPager', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdConferencing - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdConferencing function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdConferencing === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdConferencing(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdConferencing', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdConferencing('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdConferencing', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapiV10AccountAccountIdExtensionExtensionIdConferencing - errors', () => {
      it('should have a putRestapiV10AccountAccountIdExtensionExtensionIdConferencing function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapiV10AccountAccountIdExtensionExtensionIdConferencing === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.putRestapiV10AccountAccountIdExtensionExtensionIdConferencing(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-putRestapiV10AccountAccountIdExtensionExtensionIdConferencing', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.putRestapiV10AccountAccountIdExtensionExtensionIdConferencing('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-putRestapiV10AccountAccountIdExtensionExtensionIdConferencing', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdDevice - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdDevice(null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapiV10AccountAccountIdExtensionExtensionIdFax - errors', () => {
      it('should have a postRestapiV10AccountAccountIdExtensionExtensionIdFax function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapiV10AccountAccountIdExtensionExtensionIdFax === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdFax(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-postRestapiV10AccountAccountIdExtensionExtensionIdFax', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdFax('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-postRestapiV10AccountAccountIdExtensionExtensionIdFax', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumber - errors', () => {
      it('should have a postRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumber function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumber === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumber(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-postRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumber', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumber('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-postRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumber', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumber - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumber function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumber === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumber(null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumber', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumber('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumber', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdGrant - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdGrant function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdGrant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdGrant(null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdGrant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdGrant('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdGrant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdMessageStore - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdMessageStore function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdMessageStore === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdMessageStore(null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdMessageStore', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdMessageStore('fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdMessageStore', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId - errors', () => {
      it('should have a deleteRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.deleteRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-deleteRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.deleteRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-deleteRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing messageId', (done) => {
        try {
          a.deleteRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'messageId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-deleteRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing messageId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'messageId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId - errors', () => {
      it('should have a putRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.putRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-putRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.putRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-putRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing messageId', (done) => {
        try {
          a.putRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'messageId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-putRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageIdContentAttachmentId - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageIdContentAttachmentId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageIdContentAttachmentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageIdContentAttachmentId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageIdContentAttachmentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageIdContentAttachmentId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageIdContentAttachmentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing messageId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageIdContentAttachmentId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'messageId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageIdContentAttachmentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageIdContentAttachmentId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'attachmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageIdContentAttachmentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdMessageSync - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdMessageSync function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdMessageSync === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdMessageSync(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdMessageSync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdMessageSync('fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdMessageSync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdPhoneNumber - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdPhoneNumber function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdPhoneNumber === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdPhoneNumber(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdPhoneNumber', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdPhoneNumber('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdPhoneNumber', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdPresence - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdPresence function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdPresence === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdPresence(null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdPresence', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdPresence('fakeparam', null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdPresence', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdProfileImage - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdProfileImage function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdProfileImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdProfileImage(null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdProfileImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdProfileImage('fakeparam', null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdProfileImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapiV10AccountAccountIdExtensionExtensionIdProfileImage - errors', () => {
      it('should have a putRestapiV10AccountAccountIdExtensionExtensionIdProfileImage function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapiV10AccountAccountIdExtensionExtensionIdProfileImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.putRestapiV10AccountAccountIdExtensionExtensionIdProfileImage(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-putRestapiV10AccountAccountIdExtensionExtensionIdProfileImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.putRestapiV10AccountAccountIdExtensionExtensionIdProfileImage('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-putRestapiV10AccountAccountIdExtensionExtensionIdProfileImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapiV10AccountAccountIdExtensionExtensionIdProfileImage - errors', () => {
      it('should have a postRestapiV10AccountAccountIdExtensionExtensionIdProfileImage function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapiV10AccountAccountIdExtensionExtensionIdProfileImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdProfileImage(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-postRestapiV10AccountAccountIdExtensionExtensionIdProfileImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdProfileImage('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-postRestapiV10AccountAccountIdExtensionExtensionIdProfileImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdProfileImageScaleSize - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdProfileImageScaleSize function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdProfileImageScaleSize === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdProfileImageScaleSize(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdProfileImageScaleSize', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdProfileImageScaleSize('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdProfileImageScaleSize', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scaleSize', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdProfileImageScaleSize('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'scaleSize is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdProfileImageScaleSize', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapiV10AccountAccountIdExtensionExtensionIdRingout - errors', () => {
      it('should have a postRestapiV10AccountAccountIdExtensionExtensionIdRingout function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapiV10AccountAccountIdExtensionExtensionIdRingout === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdRingout(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-postRestapiV10AccountAccountIdExtensionExtensionIdRingout', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdRingout('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-postRestapiV10AccountAccountIdExtensionExtensionIdRingout', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId - errors', () => {
      it('should have a deleteRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.deleteRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-deleteRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.deleteRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-deleteRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ringoutId', (done) => {
        try {
          a.deleteRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ringoutId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-deleteRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId - errors', () => {
      it('should have a getRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ringoutId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ringoutId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapiV10AccountAccountIdExtensionExtensionIdSms - errors', () => {
      it('should have a postRestapiV10AccountAccountIdExtensionExtensionIdSms function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapiV10AccountAccountIdExtensionExtensionIdSms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdSms(null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-postRestapiV10AccountAccountIdExtensionExtensionIdSms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extensionId', (done) => {
        try {
          a.postRestapiV10AccountAccountIdExtensionExtensionIdSms('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'extensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-postRestapiV10AccountAccountIdExtensionExtensionIdSms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapiV10AccountAccountIdOrder - errors', () => {
      it('should have a postRestapiV10AccountAccountIdOrder function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapiV10AccountAccountIdOrder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.postRestapiV10AccountAccountIdOrder(null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-postRestapiV10AccountAccountIdOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdOrderOrderId - errors', () => {
      it('should have a getRestapiV10AccountAccountIdOrderOrderId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdOrderOrderId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdOrderOrderId(null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdOrderOrderId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orderId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdOrderOrderId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'orderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdOrderOrderId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdPhoneNumber - errors', () => {
      it('should have a getRestapiV10AccountAccountIdPhoneNumber function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdPhoneNumber === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdPhoneNumber(null, null, null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdPhoneNumber', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdPhoneNumberPhoneNumberId - errors', () => {
      it('should have a getRestapiV10AccountAccountIdPhoneNumberPhoneNumberId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdPhoneNumberPhoneNumberId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdPhoneNumberPhoneNumberId(null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdPhoneNumberPhoneNumberId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing phoneNumberId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdPhoneNumberPhoneNumberId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'phoneNumberId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdPhoneNumberPhoneNumberId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdRecordingRecordingId - errors', () => {
      it('should have a getRestapiV10AccountAccountIdRecordingRecordingId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdRecordingRecordingId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdRecordingRecordingId(null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdRecordingRecordingId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordingId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdRecordingRecordingId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'recordingId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdRecordingRecordingId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdRecordingRecordingIdContent - errors', () => {
      it('should have a getRestapiV10AccountAccountIdRecordingRecordingIdContent function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdRecordingRecordingIdContent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdRecordingRecordingIdContent(null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdRecordingRecordingIdContent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordingId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdRecordingRecordingIdContent('fakeparam', null, (data, error) => {
            try {
              const displayE = 'recordingId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdRecordingRecordingIdContent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10AccountAccountIdServiceInfo - errors', () => {
      it('should have a getRestapiV10AccountAccountIdServiceInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10AccountAccountIdServiceInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getRestapiV10AccountAccountIdServiceInfo(null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10AccountAccountIdServiceInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapiV10ClientInfoCustomDataCustomDataKey - errors', () => {
      it('should have a putRestapiV10ClientInfoCustomDataCustomDataKey function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapiV10ClientInfoCustomDataCustomDataKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customDataKey', (done) => {
        try {
          a.putRestapiV10ClientInfoCustomDataCustomDataKey(null, null, (data, error) => {
            try {
              const displayE = 'customDataKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-putRestapiV10ClientInfoCustomDataCustomDataKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10DictionaryCountry - errors', () => {
      it('should have a getRestapiV10DictionaryCountry function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10DictionaryCountry === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10DictionaryCountryCountryId - errors', () => {
      it('should have a getRestapiV10DictionaryCountryCountryId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10DictionaryCountryCountryId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing countryId', (done) => {
        try {
          a.getRestapiV10DictionaryCountryCountryId(null, (data, error) => {
            try {
              const displayE = 'countryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10DictionaryCountryCountryId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10DictionaryLanguage - errors', () => {
      it('should have a getRestapiV10DictionaryLanguage function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10DictionaryLanguage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10DictionaryLanguageLanguageId - errors', () => {
      it('should have a getRestapiV10DictionaryLanguageLanguageId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10DictionaryLanguageLanguageId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing languageId', (done) => {
        try {
          a.getRestapiV10DictionaryLanguageLanguageId(null, (data, error) => {
            try {
              const displayE = 'languageId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10DictionaryLanguageLanguageId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10DictionaryLocation - errors', () => {
      it('should have a getRestapiV10DictionaryLocation function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10DictionaryLocation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10DictionaryState - errors', () => {
      it('should have a getRestapiV10DictionaryState function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10DictionaryState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10DictionaryStateStateId - errors', () => {
      it('should have a getRestapiV10DictionaryStateStateId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10DictionaryStateStateId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stateId', (done) => {
        try {
          a.getRestapiV10DictionaryStateStateId(null, (data, error) => {
            try {
              const displayE = 'stateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10DictionaryStateStateId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10DictionaryTimezone - errors', () => {
      it('should have a getRestapiV10DictionaryTimezone function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10DictionaryTimezone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10DictionaryTimezoneTimezoneId - errors', () => {
      it('should have a getRestapiV10DictionaryTimezoneTimezoneId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10DictionaryTimezoneTimezoneId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timezoneId', (done) => {
        try {
          a.getRestapiV10DictionaryTimezoneTimezoneId(null, (data, error) => {
            try {
              const displayE = 'timezoneId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10DictionaryTimezoneTimezoneId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapiV10NumberParserParse - errors', () => {
      it('should have a postRestapiV10NumberParserParse function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapiV10NumberParserParse === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapiV10NumberPoolLookup - errors', () => {
      it('should have a postRestapiV10NumberPoolLookup function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapiV10NumberPoolLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapiV10NumberPoolReserve - errors', () => {
      it('should have a postRestapiV10NumberPoolReserve function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapiV10NumberPoolReserve === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapiV10Subscription - errors', () => {
      it('should have a postRestapiV10Subscription function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapiV10Subscription === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapiV10SubscriptionSubscriptionId - errors', () => {
      it('should have a deleteRestapiV10SubscriptionSubscriptionId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapiV10SubscriptionSubscriptionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subscriptionId', (done) => {
        try {
          a.deleteRestapiV10SubscriptionSubscriptionId(null, (data, error) => {
            try {
              const displayE = 'subscriptionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-deleteRestapiV10SubscriptionSubscriptionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapiV10SubscriptionSubscriptionId - errors', () => {
      it('should have a getRestapiV10SubscriptionSubscriptionId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapiV10SubscriptionSubscriptionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subscriptionId', (done) => {
        try {
          a.getRestapiV10SubscriptionSubscriptionId(null, (data, error) => {
            try {
              const displayE = 'subscriptionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-getRestapiV10SubscriptionSubscriptionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapiV10SubscriptionSubscriptionId - errors', () => {
      it('should have a putRestapiV10SubscriptionSubscriptionId function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapiV10SubscriptionSubscriptionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subscriptionId', (done) => {
        try {
          a.putRestapiV10SubscriptionSubscriptionId(null, null, null, (data, error) => {
            try {
              const displayE = 'subscriptionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-ringcentral-adapter-putRestapiV10SubscriptionSubscriptionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
