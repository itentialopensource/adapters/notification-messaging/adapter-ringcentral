## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Ringcentral. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Ringcentral.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Adapter for Ringcentral. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi(callback)</td>
    <td style="padding:15px">Get Server Info</td>
    <td style="padding:15px">{base_path}/{version}/restapi?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapiOauthAuthorize(body, callback)</td>
    <td style="padding:15px">OAuth2 Authorize</td>
    <td style="padding:15px">{base_path}/{version}/restapi/oauth/authorize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapiOauthRevoke(body, callback)</td>
    <td style="padding:15px">OAuth2 Revoke Token</td>
    <td style="padding:15px">{base_path}/{version}/restapi/oauth/revoke?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapiOauthToken(body, callback)</td>
    <td style="padding:15px">OAuth2 Get Token</td>
    <td style="padding:15px">{base_path}/{version}/restapi/oauth/token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10(callback)</td>
    <td style="padding:15px">Get API Version Info</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountId(accountId, callback)</td>
    <td style="padding:15px">Get Account Info by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdActiveCalls(accountId, direction = 'Inbound', type = 'Voice', page, perPage, callback)</td>
    <td style="padding:15px">Get Account Active (Recent) Calls</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/active-calls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdBusinessAddress(accountId, callback)</td>
    <td style="padding:15px">Get Company Business Address</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/business-address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapiV10AccountAccountIdBusinessAddress(accountId, body, callback)</td>
    <td style="padding:15px">Update Company Business Address</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/business-address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdCallLog(accountId, extensionNumber, phoneNumber, direction = 'Inbound', type = 'Voice', view = 'Simple', withRecording, dateFrom, dateTo, page, perPage, callback)</td>
    <td style="padding:15px">Get Account Call Log</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/call-log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdCallLogCallLogId(accountId, callLogId, callback)</td>
    <td style="padding:15px">Get Account Call Log Record by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/call-log/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdDepartmentDepartmentIdMembers(accountId, departmentId, page, perPage, callback)</td>
    <td style="padding:15px">Get Department Members</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/department/{pathv2}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdDevice(accountId, callback)</td>
    <td style="padding:15px">Get Account Device List</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdDeviceDeviceId(accountId, deviceId, callback)</td>
    <td style="padding:15px">Get Device by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/device/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtension(accountId, page, perPage, status = 'Enabled', type = 'User', callback)</td>
    <td style="padding:15px">Get Extension List</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionId(accountId, extensionId, callback)</td>
    <td style="padding:15px">Get Extension Info by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapiV10AccountAccountIdExtensionExtensionId(accountId, extensionId, body, callback)</td>
    <td style="padding:15px">Update Extension by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdActiveCalls(accountId, extensionId, direction = 'Inbound', type = 'Voice', page, perPage, callback)</td>
    <td style="padding:15px">Get Extension Active (Recent) Calls</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/active-calls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookSync(accountId, extensionId, syncType = 'FSync', syncToken, perPage, pageId, callback)</td>
    <td style="padding:15px">Contacts Synchronization</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/address-book-sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContact(accountId, extensionId, body, callback)</td>
    <td style="padding:15px">Create New Contact</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/address-book/contact?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContact(accountId, extensionId, startsWith, sortBy = 'FirstName', page, perPage, callback)</td>
    <td style="padding:15px">Get Contact List</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/address-book/contact?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId(accountId, extensionId, contactId, callback)</td>
    <td style="padding:15px">Delete Contact by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/address-book/contact/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId(accountId, extensionId, contactId, callback)</td>
    <td style="padding:15px">Get Contact by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/address-book/contact/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapiV10AccountAccountIdExtensionExtensionIdAddressBookContactContactId(accountId, extensionId, contactId, body, callback)</td>
    <td style="padding:15px">Update Contact by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/address-book/contact/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdAddressBookGroup(accountId, extensionId, callback)</td>
    <td style="padding:15px">Get Contact Group List</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/address-book/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdAnsweringRuleAnsweringRuleId(accountId, extensionId, answeringRuleId, callback)</td>
    <td style="padding:15px">Get Custom Answering Rule by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/answering-rule/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumber(accountId, extensionId, body, callback)</td>
    <td style="padding:15px">Add New Blocked Number</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/blocked-number?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumber(accountId, extensionId, callback)</td>
    <td style="padding:15px">Get Blocked Number List</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/blocked-number?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId(accountId, extensionId, blockedNumberId, callback)</td>
    <td style="padding:15px">Delete Blocked Number by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/blocked-number/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId(accountId, extensionId, blockedNumberId, callback)</td>
    <td style="padding:15px">Get Blocked Number by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/blocked-number/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapiV10AccountAccountIdExtensionExtensionIdBlockedNumberBlockedNumberId(accountId, extensionId, blockedNumberId, body, callback)</td>
    <td style="padding:15px">Update Blocked Number Label</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/blocked-number/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdBusinessHours(accountId, extensionId, callback)</td>
    <td style="padding:15px">Get User Hours Setting</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/business-hours?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapiV10AccountAccountIdExtensionExtensionIdCallLog(accountId, extensionId, dateTo, callback)</td>
    <td style="padding:15px">Delete Extension Call Log</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/call-log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdCallLog(accountId, extensionId, extensionNumber, phoneNumber, direction = 'Inbound', type = 'Voice', view = 'Simple', withRecording, dateTo, dateFrom, page, perPage, callback)</td>
    <td style="padding:15px">Get Extension Call Log</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/call-log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdCallLogSync(accountId, extensionId, syncType = 'FSync', syncToken, dateFrom, recordCount, statusGroup = 'Missed', callback)</td>
    <td style="padding:15px">Call Log Synchronization</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/call-log-sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdCallLogCallLogId(accountId, extensionId, callLogId, callback)</td>
    <td style="padding:15px">Get Extension Call Log Record by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/call-log/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapiV10AccountAccountIdExtensionExtensionIdCompanyPager(accountId, extensionId, body, callback)</td>
    <td style="padding:15px">Create and Send Pager Message</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/company-pager?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdConferencing(accountId, extensionId, countryId, callback)</td>
    <td style="padding:15px">Get Conferencing info</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/conferencing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapiV10AccountAccountIdExtensionExtensionIdConferencing(accountId, extensionId, body, callback)</td>
    <td style="padding:15px">Update Conferencing info</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/conferencing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdDevice(accountId, extensionId, callback)</td>
    <td style="padding:15px">Get Extension Device List</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapiV10AccountAccountIdExtensionExtensionIdFax(accountId, extensionId, body, callback)</td>
    <td style="padding:15px">Create and Send Fax Message</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/fax?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumber(accountId, extensionId, body, callback)</td>
    <td style="padding:15px">Add New Forwarding Number</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/forwarding-number?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdForwardingNumber(accountId, extensionId, page, perPage, callback)</td>
    <td style="padding:15px">Get Forwarding Numbers</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/forwarding-number?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdGrant(accountId, extensionId, page, perPage, callback)</td>
    <td style="padding:15px">Get Extension Grants</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/grant?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdMessageStore(accountId, extensionId, availability = 'Alive', conversationId, dateFrom, dateTo, direction = 'Inbound', distinctConversations, messageType = 'Fax', readStatus = 'Read', page, perPage, phoneNumber, callback)</td>
    <td style="padding:15px">Get Message List</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/message-store?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId(accountId, extensionId, messageId, purge, conversationId, callback)</td>
    <td style="padding:15px">Delete Message by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/message-store/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId(accountId, extensionId, messageId, callback)</td>
    <td style="padding:15px">Get Message by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/message-store/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageId(accountId, extensionId, messageId, body, callback)</td>
    <td style="padding:15px">Update Message by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/message-store/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdMessageStoreMessageIdContentAttachmentId(accountId, extensionId, messageId, attachmentId, callback)</td>
    <td style="padding:15px">Get Message Attachment</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/message-store/{pathv3}/content/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdMessageSync(accountId, extensionId, conversationId, dateFrom, dateTo, direction = 'Inbound', distinctConversations, messageType = 'Fax', recordCount, syncToken, syncType = 'FSync', callback)</td>
    <td style="padding:15px">Message Synchronization</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/message-sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdPhoneNumber(accountId, extensionId, usageType = 'MainCompanyNumber', page, perPage, callback)</td>
    <td style="padding:15px">Get Extension Phone Numbers</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/phone-number?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdPresence(accountId, extensionId, callback)</td>
    <td style="padding:15px">Get Extension Presence</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/presence?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdProfileImage(accountId, extensionId, callback)</td>
    <td style="padding:15px">Get Profile Image</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/profile-image?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapiV10AccountAccountIdExtensionExtensionIdProfileImage(accountId, extensionId, body, callback)</td>
    <td style="padding:15px">Update Profile Image</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/profile-image?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapiV10AccountAccountIdExtensionExtensionIdProfileImage(accountId, extensionId, body, callback)</td>
    <td style="padding:15px">Update Profile Image (same as PUT)</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/profile-image?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdProfileImageScaleSize(accountId, extensionId, scaleSize = '90x90', callback)</td>
    <td style="padding:15px">Get Scaled Profile Image</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/profile-image/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapiV10AccountAccountIdExtensionExtensionIdRingout(accountId, extensionId, body, callback)</td>
    <td style="padding:15px">Initiate RingOut Call</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/ringout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId(accountId, extensionId, ringoutId, callback)</td>
    <td style="padding:15px">Cancel RingOut Call</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/ringout/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdExtensionExtensionIdRingoutRingoutId(accountId, extensionId, ringoutId, callback)</td>
    <td style="padding:15px">Get RingOut Call Status</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/ringout/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapiV10AccountAccountIdExtensionExtensionIdSms(accountId, extensionId, body, callback)</td>
    <td style="padding:15px">Create and Send SMS Message</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/extension/{pathv2}/sms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapiV10AccountAccountIdOrder(accountId, body, callback)</td>
    <td style="padding:15px">Create New Order</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/order?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdOrderOrderId(accountId, orderId, callback)</td>
    <td style="padding:15px">Get Order by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/order/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdPhoneNumber(accountId, page, perPage, usageType = 'MainCompanyNumber', callback)</td>
    <td style="padding:15px">Get Account Phone Numbers</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/phone-number?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdPhoneNumberPhoneNumberId(accountId, phoneNumberId, callback)</td>
    <td style="padding:15px">Get Phone Number by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/phone-number/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdRecordingRecordingId(accountId, recordingId, callback)</td>
    <td style="padding:15px">Get Call Recording Metadata</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/recording/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdRecordingRecordingIdContent(accountId, recordingId, callback)</td>
    <td style="padding:15px">Get Call Recording Content</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/recording/{pathv2}/content?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10AccountAccountIdServiceInfo(accountId, callback)</td>
    <td style="padding:15px">Get Account Service Info</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/account/{pathv1}/service-info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapiV10ClientInfoCustomDataCustomDataKey(customDataKey, body, callback)</td>
    <td style="padding:15px">Update Custom Data by Key</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/client-info/custom-data/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10DictionaryCountry(loginAllowed, numberSelling, page, perPage, callback)</td>
    <td style="padding:15px">Get Country List</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/dictionary/country?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10DictionaryCountryCountryId(countryId, callback)</td>
    <td style="padding:15px">Get Country by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/dictionary/country/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10DictionaryLanguage(callback)</td>
    <td style="padding:15px">Get Supported Language List</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/dictionary/language?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10DictionaryLanguageLanguageId(languageId, callback)</td>
    <td style="padding:15px">Get Language by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/dictionary/language/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10DictionaryLocation(orderBy = 'Npa', page, perPage, stateId, withNxx, callback)</td>
    <td style="padding:15px">Get Location List</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/dictionary/location?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10DictionaryState(countryId, page, perPage, withPhoneNumbers, callback)</td>
    <td style="padding:15px">Get State/Province List</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/dictionary/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10DictionaryStateStateId(stateId, callback)</td>
    <td style="padding:15px">Get State/Province by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/dictionary/state/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10DictionaryTimezone(page, perPage, callback)</td>
    <td style="padding:15px">Get Time Zone List</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/dictionary/timezone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10DictionaryTimezoneTimezoneId(timezoneId, callback)</td>
    <td style="padding:15px">Get Time Zone by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/dictionary/timezone/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapiV10NumberParserParse(homeCountry, nationalAsPriority, body, callback)</td>
    <td style="padding:15px">Parse Phone Number</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/number-parser/parse?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapiV10NumberPoolLookup(areaCode, countryCode, countryId, exclude, extendedSearch, line, numberPattern, nxx, npa, paymentType = 'TollFree', perPage, smsEnabled, callback)</td>
    <td style="padding:15px">Look up Phone Number</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/number-pool/lookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapiV10NumberPoolReserve(body, callback)</td>
    <td style="padding:15px">Reserve Phone Number</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/number-pool/reserve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapiV10Subscription(aggregated, body, callback)</td>
    <td style="padding:15px">Create New Subscription</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/subscription?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapiV10SubscriptionSubscriptionId(subscriptionId, callback)</td>
    <td style="padding:15px">Cancel Subscription by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/subscription/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapiV10SubscriptionSubscriptionId(subscriptionId, callback)</td>
    <td style="padding:15px">Get Subscription by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/subscription/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapiV10SubscriptionSubscriptionId(subscriptionId, aggregated, body, callback)</td>
    <td style="padding:15px">Update/Renew Subscription by ID</td>
    <td style="padding:15px">{base_path}/{version}/restapi/v1.0/subscription/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
