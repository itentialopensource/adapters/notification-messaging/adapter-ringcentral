
## 0.3.4 [10-15-2024]

* Changes made at 2024.10.14_20:03PM

See merge request itentialopensource/adapters/adapter-ringcentral!11

---

## 0.3.3 [09-16-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-ringcentral!9

---

## 0.3.2 [08-14-2024]

* Changes made at 2024.08.14_18:11PM

See merge request itentialopensource/adapters/adapter-ringcentral!8

---

## 0.3.1 [08-07-2024]

* Changes made at 2024.08.06_19:24PM

See merge request itentialopensource/adapters/adapter-ringcentral!7

---

## 0.3.0 [07-23-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/notification-messaging/adapter-ringcentral!6

---

## 0.2.3 [03-29-2024]

* Changes made at 2024.03.29_10:19AM

See merge request itentialopensource/adapters/notification-messaging/adapter-ringcentral!5

---

## 0.2.2 [03-11-2024]

* Changes made at 2024.03.11_11:12AM

See merge request itentialopensource/adapters/notification-messaging/adapter-ringcentral!4

---

## 0.2.1 [02-28-2024]

* Changes made at 2024.02.28_12:30PM

See merge request itentialopensource/adapters/notification-messaging/adapter-ringcentral!3

---

## 0.2.0 [01-01-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/notification-messaging/adapter-ringcentral!1

---

## 0.1.1 [06-09-2022]

* Bug fixes and performance improvements

See commit 6693853

---

## 0.1.1 [09-09-2021]

- Initial Commit

See commit 99b66f24

---
