# Adapter for Ringcentral

Vendor: Ringcentral
Homepage: https://www.ringcentral.com/

Product: Ringcentral
Product Page: https://www.ringcentral.com/

## Introduction
We classify Ringcentral into the Notification domain as it provides a cloud-based communication and collaboration solutions for businesses, including voice, video, messaging, and contact center services.

## Why Integrate
The Ringcentral adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Ringcentral. With this adapter you have the ability to perform operations such as:

- Send an email message

## Additional Product Documentation
The [API documents for Ringcentral](https://netstorage.ringcentral.com/dpw/api-reference/specs/public/office/rc-platform.yml?v=2024072220240628-0704)